# Hestia

This is the frontend for my personal website, [distant.land](distant.land). Years back, my personal website was a Scala Play server, called "Gaea", and "Hestia" is intended to be a spiritual successor to it.

# License

This is for my personal use, but it's MIT licensed—feel free to take anything useful to you according to the license :)

# Design Decisions

- Abandoned my mostly-working backend in favor of a more modular self-hosted approach:

  - [Ghost](https://ghost.org/) as a headless blocking platform
  - [Plausible](https://plausible.io/) for privacy-friendly analytics
  - [Commento++](https://github.com/souramoo/commentoplusplus) comments (kind of buggy, will likely replace)

- Migrated from Redux/RTK -> Zustand
