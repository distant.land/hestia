import { Card } from "@caldwell619/react-kanban";

export interface DevelopCard extends Card {
  priority: "unprioritized" | "low" | "medium" | "high";
}
