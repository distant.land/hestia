/* eslint-disable @typescript-eslint/no-unused-vars */
declare module "tiny-relative-date" {
  const relativeDate = (from: Date, to?: Date) => string;

  export default relativeDate;
}
