import { Button, ButtonProps } from "@mui/material";
import omit from "lodash.omit";
import spinningBox from "../assets/spinning-box.gif";

interface LoadingButtonProps extends ButtonProps {
  text: string;
  loadingText?: string;
  loading: boolean;
}

export default function LoadingButton(props: LoadingButtonProps) {
  const underlyingProps = omit(props, ["text", "loadingText", "loading"]);

  return (
    <Button
      {...underlyingProps}
      sx={{
        ...underlyingProps.sx,
        //position: "relative",
        backgroundColor: "rgb(0, 123, 255)",
        color: "#fff",
        "&:disabled": {
          color: "#fff",
          backgroundColor: "gray"
        },
        "&:hover": {
          backgroundColor: "rgb(0, 123, 255)",
          transition: "box-shadow 1s"
        },
        "&::after": {
          content: '""',
          position: "absolute",
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          boxShadow: "0 0 10px 0 #3498db inset, 0 0 10px 4px #3498db",
          opacity: 0 /* hide the hover shadow */,
          transition: "opacity 0.4s ease-out"
        },
        "&:hover::after": {
          opacity: 1 /* show the hover shadow */
        }
      }}
      disabled={props.loading}
    >
      {props.loading ? props.loadingText || props.text : props.text}
      {props.loading && (
        <img
          style={{ width: "2em" }}
          src={spinningBox}
          alt="loading animation"
        />
      )}
    </Button>
  );
}
