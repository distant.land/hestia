import { ButtonBase, ButtonBaseProps } from "@mui/material";
import { TouchRippleActions } from "@mui/material/ButtonBase/TouchRipple";
import { useRef } from "react";

export type CustomButtonBaseProps = ButtonBaseProps;

export default function CustomButtonBase(props: CustomButtonBaseProps) {
  const touchRippleRef = useRef<TouchRippleActions>(null);

  return (
    <ButtonBase
      {...props}
      sx={{
        color: "black",
        "&:hover": {
          backgroundColor: "inherit"
        },
        "& .MuiTouchRipple-root .MuiTouchRipple-ripple": {
          left: "unset !important",
          right: 0,
          width: "100% !important"
        },
        ...props.sx
      }}
      touchRippleRef={touchRippleRef}
      onMouseDown={() => touchRippleRef.current?.stop()}
      onMouseEnter={() => touchRippleRef.current?.pulsate()}
      onMouseLeave={() => touchRippleRef.current?.stop()}
    />
  );
}
