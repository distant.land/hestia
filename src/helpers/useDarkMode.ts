import useUIState from "./useUIState";

export default function useDarkMode() {
  const darkMode = useUIState((state) => state.darkMode);
  return darkMode;
}
